
import java.util.Scanner;
public class UTS021200039 {
    public static void main(String[] args) {
        Scanner np = new Scanner (System.in);
        String hasil, nama, npm;
        int nilai_tugas,nilai_uts,nilai_uas;
        double nilai_akhir;
        System.out.print("Nama Anda : ");
        nama = np.nextLine();
        System.out.print("NPM Anda : ");
        npm = np.nextLine();
        System.out.print("Nilai Tugas Anda : ");
        nilai_tugas = np.nextInt();
        System.out.print("Nilai UTS Anda : ");
        nilai_uts = np.nextInt();
        System.out.print("Nilai UAS Anda : ");
        nilai_uas = np.nextInt();
        System.out.println("===== Data Telah Diinput Sedang Diproses =====");
        System.out.println("Sabar Om Lagi Loading......");
        
        nilai_tugas = (int)(nilai_tugas*0.40);
        nilai_uts = (int)(nilai_uts*0.25);
        nilai_uas = (int)(nilai_uas*0.35);
        nilai_akhir =nilai_tugas+nilai_uts+nilai_uas;
        if(nilai_akhir >= 85 && nilai_akhir <= 100){
            hasil =("Lulus Dengan Grade A");
        }else if(nilai_akhir >= 70 && nilai_akhir <85){
            hasil ="Lulus Dengan Grade B";
        }else if(nilai_akhir >= 60 && nilai_akhir <70){
            hasil ="Lulus Dengan Grade C";
        }else if(nilai_akhir >= 50 && nilai_akhir <60){
            hasil ="Tidak Lulus Dengan Grade D";
        }else{
            hasil ="Tidak Lulus Dengan Grade E";
        }
        System.out.println("===== Hasil Nilai Akhir =====");
        System.out.println("Atas Nama : "+nama);
        System.out.println("Dengan NPM : "+npm);
        System.out.println("Nilai Akhir : "+nilai_akhir);
        System.out.println(hasil);
        
    }
 
}